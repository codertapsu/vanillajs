'use strict';
import 'style.css';
function equal(input) {
  let value = input.replace(/\++/gim, '+').replace(/\-+/gim, '-').replace(/\*+/gim, '*').replace(/\/+/gim, '/').replace(/\.+/gim, '.').replace(/\.$/, '');
  return eval(value);
}
(function () {
  import ApiService from './services/api.service';
  const buttons = document.querySelectorAll('button.btn');
  buttons.forEach((btn) => {
    btn.addEventListener('click', (e) => {
      const rect = e.target.getBoundingClientRect();
      const x = e.clientX - rect.left;
      const y = e.clientY - rect.top;
      const ripples = document.createElement('span');
      ripples.classList.add('ripples');
      ripples.style.left = x + 'px';
      ripples.style.top = y + 'px';
      btn.appendChild(ripples);
      setTimeout(() => {
        ripples.remove();
      }, 1000);
    });
  });

  const progressBar = document.getElementById('progress-bar');
  const totalHeight = document.body.scrollHeight - window.innerHeight;
  window.onscroll = () => {
    const progressHeight = (window.pageYOffset / totalHeight) * 100;
    progressBar.style.height = progressHeight + '%';
  };

  const deg = 6;
  const hour = document.getElementById('hr');
  const min = document.getElementById('mn');
  const sec = document.getElementById('sc');
  const intervalClock = setInterval(() => {
    let day = new Date();
    let hh = day.getHours() * 30;
    let mm = day.getMinutes() * deg;
    let ss = day.getSeconds() * deg;
    hour.style.transform = `rotateZ(${hh + mm / 12}deg)`;
    min.style.transform = `rotateZ(${mm}deg)`;
    sec.style.transform = `rotateZ(${ss}deg)`;
  });

  window.addEventListener('beforeunload', function (e) {
    clearInterval(intervalClock);
    return true;
  });

  document.onvisibilitychange = () => {
    const isVisible = document.visibilityState === 'visible';
    document.title = isVisible ? 'Calculator' : document.visibilityState;
  };
})();
