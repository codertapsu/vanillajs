const App = async () => {
  const template = document.createElement('template');
  template.innerHTML = `
  <div>Hello</div>
  `;
  return template.content.cloneNode(true);
};

export default App;
