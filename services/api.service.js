const ApiService = (() => {
  let instance;
  const init = () => {
    const post = () => {};
    const get = () => {
      console.log('get');
    };
    const put = () => {};
    const patch = () => {};
    const remove = () => {};
    return {
      post,
      get,
      put,
      patch,
      remove,
    };
  };
  return {
    getI: () => {
      if (!instance) {
        instance = init();
      }
      return instance;
    },
  };
})();

export default ApiService;
