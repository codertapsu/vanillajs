const deg = 6;
const hour = document.getElementById('hr');
const min = document.getElementById('mn');
const sec = document.getElementById('sc');
const intervalClock = setInterval(() => {
  let day = new Date();
  let hh = day.getHours() * 30;
  let mm = day.getMinutes() * deg;
  let ss = day.getSeconds() * deg;
  hour.style.transform = `rotateZ(${hh + mm / 12}deg)`;
  min.style.transform = `rotateZ(${mm}deg)`;
  sec.style.transform = `rotateZ(${ss}deg)`;
});

window.addEventListener('beforeunload', function (e) {
  clearInterval(intervalClock);
  return true;
});
