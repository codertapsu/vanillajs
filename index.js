import './style.css';
import App from './app';
(async () => {
  document.getElementById('app-root').appendChild(await App());
})();
